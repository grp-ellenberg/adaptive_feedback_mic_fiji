/***
 * @author Antonio Politi
 * segmentParticles using only one channel
 * This plugin does:
 * 1. Median filer with radius med_radius (fixed)
 * 2. Gaussian blur radius gauss_radius (fixed)
 * 3. Threshold 
 * 4. Particle analyze (exclude particles smaller than min_size), fill holes. 
 * 5. Eventually erode
 * 6. Eventually watershed if area > max_size
 * Size can be given in pixel or um2 (if the scale of the image has been set)
 */

import ij.*;
import ij.plugin.*;
import ij.measure.*;
import ij.plugin.frame.RoiManager;
import ij.plugin.ZProjector;
import ij.gui.GenericDialog;
import ij.plugin.filter.Analyzer;
import ij.plugin.filter.ParticleAnalyzer;
import ij.plugin.filter.EDM;
import java.io.*;

public class segment_Particles implements PlugIn {
	
	String plugin_name = "Segment Particles";
	
	public void run(String arg) {
		IJ.log(" ");
		IJ.log(""+plugin_name+": started.");
 		String[] methods = {"Default","Triangle", "Mean", "Huang", "Otsu"};
		String[] units = {"px2", "um2"};
		String[] projections = {"MAX", "SUM", "STD"};
		String[] channels = {"C1", "C2", "C3", "C4"};
    	GenericDialog gd = new GenericDialog("Segment particles");
    	gd.addChoice("Threshold method: ", methods, methods[0]);
    	gd.addChoice("units: ", units, units[1]);
    	gd.addChoice("projection (if stack): ", projections, projections[0]);
    	gd.addChoice("channel (if multichannel): ", channels, channels[0]);
    	gd.addCheckbox("Exclude boundary", true);
    	gd.addNumericField("Minimal size: ", 50, 1);
    	gd.addNumericField("Maximal size : ", 600, 1);
    	gd.addNumericField("Minimal size (for watersed): ", 200, 1);
    	

    	gd.addNumericField("Watershed operations:", 1, 0);
    	gd.addNumericField("Filter1 median:", 0, 1);
    	gd.addNumericField("Filter2 gauss:", 0, 1);
    	gd.addCheckbox("Show binary", false);
    	gd.showDialog();
    	
    	if(gd.wasCanceled()) return;
    	
    	String method = (String)gd.getNextChoice();
    	String unit = (String)gd.getNextChoice();
    	String projection = (String) gd.getNextChoice();
    	String channel = (String) gd.getNextChoice();
    	
    	boolean exboundary = (boolean) gd.getNextBoolean();
    	float min_size = (float)gd.getNextNumber();
    	float max_size = (float)gd.getNextNumber();
    	float min_size_ws = (float)gd.getNextNumber();
    	int watershed_nr = (int)gd.getNextNumber();
    	float filter_radius_median = (float)gd.getNextNumber();
    	float filter_radius_gauss = (float)gd.getNextNumber();
    	boolean show = (boolean) gd.getNextBoolean();
    	//prepare some settings for thresholding 
    	IJ.log(""+plugin_name+": Setting for thresholding.");
    	IJ.setForegroundColor(255, 255, 255);
		IJ.setBackgroundColor(0, 0, 0);
		IJ.run("Set Measurements...","area");
		IJ.run("Options...", "iterations=1 count=1 black edm=Overwrite");
		IJ.run("ROI Manager...", "");
	
		// Variable to contain images
    	ImagePlus imp;
    	ImagePlus impPro;
    	ImagePlus impSeg;
    	
		imp = IJ.getImage();
		String title = imp.getTitle();
		
		if ( !imp.getCalibration().getUnit().equals("\u00B5"+"m") && unit.equals("um2")) {
			IJ.log(""+plugin_name + ": ERROR image has not \u00B5"+"m calibration. Please use has unit px2.");
			return;
		}
		impPro = imp.duplicate();
		impPro = preprocessImage(impPro, projection, channel, filter_radius_median, filter_radius_gauss );

		impPro.setTitle("SEG_"+title);
		if (show)
			impPro.show();
		
		impPro.copyScale(imp);
		
		if (unit.equals("px^2")) {
			IJ.run(impPro, "Set Scale...", "distance=1 known=1 pixel=1 unit=px");
			min_size = (float) (min_size/(imp.getCalibration().pixelWidth*imp.getCalibration().pixelWidth ));
			max_size = (float) (max_size/(imp.getCalibration().pixelWidth*imp.getCalibration().pixelWidth ));
			min_size_ws = (float) (min_size_ws/(imp.getCalibration().pixelWidth*imp.getCalibration().pixelWidth ));
		}
		//IJ.log(""+plugin_name + ": min_size : " + min_size + ", max_size: " + max_size);
		impSeg = impPro.duplicate();

		impSeg.setTitle("SEG_BIN_"+title);

		if ( show )
			impSeg.show();
		
		impSeg.copyScale(imp);
		
		impSeg = segmentParticles(impSeg, method,  min_size, max_size, min_size_ws, watershed_nr, exboundary);
		RoiManager manager; 
		manager =  RoiManager.getInstance();
		manager.runCommand("Show All");
		manager.runCommand("Show None");
			
	}
	
	/**
	 * preprocessImage
	 * checks format of image and does median filter on all stacks. 
	 * Perform maximal projection
	 * perform gaussian blur 
	 * convert to 8 bit if needed
	 * @param imp 				image to process (and also changed)
	 * @param filter_radius  	radius for median and gaussian filter
	 */
	public ImagePlus preprocessImage(ImagePlus imp, String projection, String channel, float filter_radius_median, float filter_radius_gauss) {
		
		int channelNr;
		channelNr = 1;
		for (int i = 1; i < 5; i++) {
			if (channel.contains(""+i )) {
				channelNr = i;
			}		
		}
	
		//retImp = imp.duplicate();
		ZProjector zp = new ZProjector();
		//get first channel
		if ( imp.isHyperStack() ) {
			if ( channelNr <= imp.getNChannels()  ) {
				imp = new Duplicator().run(imp, channelNr, channelNr, 1, imp.getNSlices(), 1, imp.getNFrames());
				IJ.log(""+plugin_name +": WARNING image has more than one channel. Use as default only first channel");
			} 
		}
		
		//do maximal projection and filtering
		if ( filter_radius_median > 0 ) {
			//IJ.log(""+ plugin_name + ": Median filter");
			IJ.run(imp, "Median...", "radius="+filter_radius_median+" stack");
		}
		
		if ( imp.getNSlices() > 1 ) {
			//IJ.log(""+ plugin_name + ": Maximal projetion");
			if ( projection.contains("MAX") ) 
					zp.setMethod(zp.MAX_METHOD);
			if ( projection.contains("SUM") ) 
				zp.setMethod(zp.SUM_METHOD);
			if ( projection.contains("STD") ) 
				zp.setMethod(zp.SD_METHOD);
			zp.setImage(imp);
			zp.doProjection();
			imp = zp.getProjection();
		}
		if (filter_radius_gauss > 0) {
			//IJ.log(""+plugin_name + ": Gauss blur");
			IJ.run(imp, "Gaussian Blur...", "radius="+filter_radius_gauss+" stack");
		}
		//convert to 8-bit
		if ( imp.getType() != imp.GRAY8 ) {
			IJ.log(""+plugin_name +": Convert to 8-bit");
			IJ.run(imp, "Enhance Contrast", "saturated=0.35");
			IJ.run(imp, "8-bit", "");
		}
		return imp;
		
	}
	
	/**
	 * resetManager()
	 * clear ROI manager and result table	 
	 * */
	private void resetManager(RoiManager manager, ResultsTable rt) {
		//RoiManager manager; 
		//ResultsTable rt; 
		//manager =  RoiManager.getInstance();
		//rt = ResultsTable.getResultsTable();
		if ( manager != null ) {
			manager.runCommand("reset");
			manager.runCommand("Show All");
			manager.runCommand("Show None");
		}
		if ( rt != null )
			rt.reset();
	}

	/**
	 * segmentParticles
	 * perform Autothreshold according to method
	 * fill holes
	 * analyze particles from min_size up to 4*max_size
	 * perform watershed on particles larger than max_size
	 * exclude particle on boundary only after first watershed
	 * @param impPro 	An image
	 * @param method 	Method for Auto Threshold
	 * @param  min_size	min_size of particles
	 * @param  max_size  max_size of particles
	 * @param  erode_nr  nr of erode operations performed on each single object after first watershed
	 * */
	public ImagePlus segmentParticles(ImagePlus imp, String method, float min_size, float max_size, float min_size_ws, int watershed_nr, boolean exboundary) {
		//ParticleAnalyzer PA;
		int[] roiIndexes;
		String exclude;
		RoiManager manager; 
		ResultsTable rt; 
		ParticleAnalyzer PA;
		ParticleAnalyzer PAEx;
		EDM ws;
		
		manager =  new RoiManager(true);
		//manager = RoiManager.getInstance();
		rt = new ResultsTable();
		//rt = ResultsTable.getResultsTable();
		//convert to pixels
		double min_size_px = (float) (min_size/(imp.getCalibration().pixelWidth*imp.getCalibration().pixelWidth ));
		double max_size_px = (float) (max_size/(imp.getCalibration().pixelWidth*imp.getCalibration().pixelWidth ));
		double min_size_ws_px = (float) (min_size_ws/(imp.getCalibration().pixelWidth*imp.getCalibration().pixelWidth ));
		//IJ.log("Max size in pixel: " + max_size_px);
		int options;
		int optionsEx;
		int measure = ParticleAnalyzer.AREA; 
		options = ParticleAnalyzer.ADD_TO_MANAGER + ParticleAnalyzer.CLEAR_WORKSHEET + ParticleAnalyzer.SHOW_NONE +  ParticleAnalyzer.INCLUDE_HOLES; 
		optionsEx = options + ParticleAnalyzer.EXCLUDE_EDGE_PARTICLES;
		PA = new ParticleAnalyzer(options, measure, rt, min_size_px, max_size_px);
		PAEx = new ParticleAnalyzer(optionsEx, measure, rt, min_size_px, max_size_px );
		ParticleAnalyzer.setRoiManager(manager);
		
		int nrParticles;
		
		ImagePlus imp_roi; 
		
		IJ.run(imp, "Auto Threshold", "method="+method+" white stack");
		IJ.run(imp, "Fill Holes", "stack");
		IJ.run(imp,"Open","");
	
		//perform a watershed on particles that are larger than a certain size	
		for (int i=0; i < watershed_nr; i++) {			
			//IJ.log("watershed round " + i);
			resetManager(manager, rt);
			ParticleAnalyzer.setRoiManager(manager);

			imp.deleteRoi();
			analyzer(PAEx, PA, imp, (exboundary && i> 0));


			if ( manager == null || rt == null) {
				IJ.log("segment_particles: ERROR no ROI manager or results Table");
				return imp;
			}
						
			nrParticles = manager.getCount();
			if ( nrParticles == 0 ) { 
				IJ.log(""+plugin_name + ": WARNING No particle has been segmented");
				return imp;
			}

			//do one erode operation after first watershed (this may be removed)

			imp.deleteRoi();
			imp_roi = new Duplicator().run(imp);
			IJ.run(imp_roi, "Watershed", "");
			for (int j = 0; j < nrParticles; j++){
				imp.deleteRoi();
				imp_roi.deleteRoi();
				//check for large objects
				if ( rt.getValue("Area", j) > min_size_ws_px ) {
					IJ.log(""+plugin_name + ": Watershed of ROI "+ j);
					manager.select(imp_roi, j);
					IJ.run(imp_roi,"Copy","");
					manager.select(imp, j);
					IJ.run(imp,"Paste","");
				}
			}
			imp_roi.close();
		}
		
		
		resetManager(manager, rt);
		imp.deleteRoi();
		manager = RoiManager.getInstance();
		resetManager(manager, rt);
		manager = RoiManager.getInstance();
		ParticleAnalyzer.setRoiManager(manager);
		PAEx = new ParticleAnalyzer(ParticleAnalyzer.ADD_TO_MANAGER + ParticleAnalyzer.CLEAR_WORKSHEET + 
				ParticleAnalyzer.EXCLUDE_EDGE_PARTICLES + ParticleAnalyzer.IN_SITU_SHOW + ParticleAnalyzer.SHOW_MASKS, measure, rt, min_size_px, max_size_px );
		PA = new ParticleAnalyzer(ParticleAnalyzer.ADD_TO_MANAGER + ParticleAnalyzer.CLEAR_WORKSHEET + ParticleAnalyzer.IN_SITU_SHOW + ParticleAnalyzer.SHOW_MASKS, measure, rt, min_size_px, max_size_px );
		analyzer(PAEx, PA, imp, exboundary);
		if ( exboundary	)
			return PAEx.getOutputImage();
		else
			return PA.getOutputImage();
	}
	
	/*
	 * choose between two Particle analyzer depending on exclude
	 */
	private void  analyzer(ParticleAnalyzer PAEx, ParticleAnalyzer PA, ImagePlus imp, boolean exclude){
		if ( exclude )
			PAEx.analyze(imp);
		else 
			PA.analyze(imp);
	}
	
	private int[] makeIntArray(int start, int end) {
		if (end >= start) {
			int length = end-start+1;
			int[] array = new int[end-start+1];
			for ( int i=0; i< length; i++) {
				array[i] = start + i;
			}
			return array;
		} else {
			return null;
		}
	}

}



