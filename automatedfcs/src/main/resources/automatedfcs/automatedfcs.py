"""
automatedfcs.py
Utility to interact with Zeiss microscopes using the MyPiC VBA macro (ZEN black)
The macro monitors a directory where MyPiC save files.
When a file matches a pipeline and task ID the file is processed.
Bright objects are segmented and coordinates extracted.
This coordinates + a command are written in the WindowsRegistry for MyPiC.
MyPiC can then start a different pipeline. 
"""

import sys
import os
# local modification if Automated_FCS.jar is not used
from sys import path
from java.lang.System import getProperty
path.append(os.path.join(getProperty('fiji.dir'), 'plugins', 'EMBL'))

from ij import IJ
from ij.plugin import ChannelSplitter
import segment_Particles
from ij.plugin.frame import RoiManager
from ij.measure import Measurements
from ij.io import OpenDialog
from ij import ImagePlus
from ij.plugin import RGBStackMerge
import automatedImgTools as autTool
import os.path
import time
import copy
import random
from ij.plugin import ZProjector
from loci.plugins import BF
from loci.plugins.in import ImporterOptions
from Message import Message
from zeiss import ZeissKeys
from fileMonitoring import DialogParameters
class LocDialogParameters(DialogParameters):
    """
     When the class is initiated it will open directly
    """
    COMBO_FIELDS = dict(pip=[['Pipeline'], ['None', 'Default', 'Trigger1', 'Trigger2']],
                        task=[['Task'], ["1", "2", "3", "4", "5"]],
                        command=[['Command'],
                                 [Message.CODE_NOTHING, Message.CODE_FOCUS, Message.CODE_SETFCSPOS,
                                  ';'.join([Message.CODE_SETFCSPOS, Message.CODE_FOCUS]),
                                  Message.CODE_TRIGGER1, Message.CODE_TRIGGER2]],
                        channel_seg=[['Channel segmentation'], ["1", "2", "3", "4"]],
                        seg_method=[['Seg. method'], ["Default", "Triangle", "Li", "MaxEntropy", "Otsu", "Shanbhag"]],
                        filter_rad=[['filer radius (px)'], [2]],
                        area_max=[['Exclude objects > (um2)'], [600]],
                        area_min=[['Exclude objects < (um2)'], [100]],
                        area_ws=[['Watershed if > (um2)'], [300]],
                        exclude_bnd=[['Exclude boundaries'], ["Yes", "No"]],
                        channel_thr1=[['Channel intensity filter1'], ["None", "1", "2", "3", "4", "5", "6"]],
                        intensity_min1=[['Min intensity'], [0]],
                        intensity_max1=[['Max intensity'], [255]],
                        channel_thr2=[['Channel intensity filter2'], ["None", "1", "2", "3", "4", "5", "6"]],
                        intensity_min2=[['Min intensity'], [0]],
                        intensity_max2=[['Max intensity'], [255]],
                        nrParticles=[['Number of particles'], [1]],
                        pick_method=[["Pick particle in"], ["center", "random"]],
                        fcs_inside=[['FCS pts. region1 (inside)'], ['0', '1', '2', '3', '4', '5', '6', '7', '8']],
                        pixel_erode=[['# oper. (erode <0, dilate >0)'], [-13]],
                        fcs_outside=[['FCS pts. outside obj.'], ['0', '1', '2', '3', '4', '5', '6', '7', '8']],
                        pixel_dilate=[['# oper. (erode <0, dilate >0)'], [5]],
                        fcs_z=[["Update z-pos for FCS"], ["No", "Yes"]])

    # order in which parameters should be shown
    COMBO_ORDER = ['pip', 'task', 'command', 'channel_seg', 'seg_method', 'filter_rad', 'area_max', 'area_min',
                   'area_ws', 'exclude_bnd', 'channel_thr1', 'intensity_min1', 'intensity_max1',
                   'channel_thr2', 'intensity_min2', 'intensity_max2', 'nrParticles', 'pick_method',
                   'fcs_inside', 'pixel_erode', 'fcs_outside', 'pixel_dilate', 'fcs_z']
    COMBO_SEPARATOR = [2, 9, 12, 15, 17]
    JOBS_DICT = {'None': '', 'Default': 'DE_', 'Trigger1': 'TR1_', 'Trigger2': 'TR2_'}
    nrOfJobs = 3
    nrSeparators = 5
    rois = [None] * nrOfJobs
    Segs = [None] * nrOfJobs
    Procs = [None] * nrOfJobs

    def __init__(self, macroname):
        super(LocDialogParameters, self).__init__(macroname)

    def consistencycheck(self):
        """
        function that can be overidden in implementation. Should check for consitency of parameters and issue a warning
        """

        for jobl in self.jobs:
            nrfcspts = int(jobl['fcs_inside']) + int(jobl['fcs_outside'])
            nrparticles = int(jobl['nrParticles'])
            if jobl['pip'] == 'None':
                continue
            if jobl['command'] in ('setFcsPos', 'setFcsPos;focus'):
                if nrparticles > 1:
                    IJ.showMessage('Parameter consistency',
                                   'Number of particles %d\nThe command \'setFcsPos\' applies only for a single particle.\n Change Number of particles to 1' % nrparticles)
                if nrfcspts == 0:
                    IJ.showMessage('Parameter consistency',
                                   "Number of FCS points = %d\nThe command \'setFcsPos\' requires FCS points.\nSet 'FCS pts. region1 (inside)' or 'FCS pts. region2 (outside)' > 0" % nrfcspts)
            else:
                if nrfcspts > 0:
                    IJ.showMessage('Parameter consistency',
                                   "Number of FCS points = %d\n Command is \'%s\'\n Change the command to  'setFcsPos' or 'setFcsPos;focus'" % (
                                       nrfcspts, jobl['command']))

    def openFile(self, filepath, imgPrefix, force=False):
        """
        openfile with img prefix and split channels returns Channel splitted ImageJ plus pixelSize filepath and filaname
        :param filepath: path to file
        :param imgPrefix: character string to be matched with filepath
        :param force: force the opening even if it does not match the prefix
        :return:
        """
        IJ.log("imgPrefix " + imgPrefix)

        if filepath == '':
            od = OpenDialog("Choose image file", None)
            if od.getFileName() is None:
                return None
            filepath = os.path.join(od.getDirectory(), od.getFileName())
        filename = os.path.splitext(os.path.basename(filepath))
        # and ('.lsm' in filename or '.czi' in filename)

        if imgPrefix is not '' and (imgPrefix in filename[0] or force) and (
                        '.lsm' == filename[1] or '.czi' == filename[1]):
            IJ.run("Close All", "")
            IJ.run("Clear Results")
            time.sleep(1)
            options = ImporterOptions()
            options.setId(filepath)
            options.setTBegin(0,0)
            options.setTEnd(0,0)
            images = BF.openImagePlus(options)
            # this opens also czi images
            image = images[0]

            if image is None:
                raise NameError('Fiji failed to open image ' + filepath)

            pixelSize = image.getCalibration().pixelWidth
            IJ.run(image, "Set Scale...", "distance=0 known=0 pixel=1 unit=pixel")
            LUTs = image.getLuts()

            if image.getNChannels() > 1:
                chSp = ChannelSplitter()
                imageC = chSp.split(image)
            else:
                imageC = [image.duplicate()]
            return [imageC, pixelSize, filepath, filename, LUTs]
        else:
            IJ.log("Nothing to do for " + filepath)
            return None

    def runOnFile(self, afile, show=True, jobnr=None):
        afile = str(afile)
        if afile == '' or afile is None:
            od = OpenDialog("Choose image file", None)
            if od.getFileName() is None:
                return
            afile = os.path.join(od.getDirectory(), od.getFileName())
        if '.log' in afile:
            return
        try:
            zk = ZeissKeys()
            msg = Message()
            if self.jobs is None:
                IJ.showMessage("First set the parameters!")
            if all([job['pip'] == 'None' for job in self.jobs]):
                IJ.showMessage("Nothing to do! At least on pipeline must be different than None!")
                return 0
            # this is necessary to change value of jobs (TODO: check it again)
            jobs = copy.deepcopy(self.jobs)
            random.seed()
            for ijob, job in enumerate(jobs):
                jobl = job
                if jobl['pip'] == 'None':
                    continue
                # get the images from afile
                if jobnr is not None:
                    if jobnr == ijob:
                        # force opening
                        imageDef = self.openFile(afile, self.JOBS_DICT[jobl['pip']] + jobl['task'] + "_", True)
                    else:
                        continue
                else:
                    imageDef = self.openFile(afile, self.JOBS_DICT[jobl['pip']] + jobl['task'] + "_")

                jobl['channel_seg'] = int(jobl['channel_seg'])
                if imageDef is None:
                    continue
                [imageC, pixelSize, filepath, filename, LUTs] = imageDef
                # consistency checks
                if jobl['channel_seg'] > len(imageC):
                    raise IOError('Image has only %d channels. Channel for segmentation is %d' % (
                        len(imageC), jobl['channel_seg']))

                # create a roiManager in case one is missing
                roim = RoiManager.getInstance()
                if roim is None:
                    roim = RoiManager()
                roim.runCommand("reset")
                self.msgLabel2.setText(afile)

                # clean up error messages
                IJ.run("Read Write Windows Registry",
                       "action=write location=[HKCU\\%s] key=%s value=[""] windows=REG_SZ" % (
                           zk.regkey, zk.subkey_errormsg))

                # re-initialize variables
                self.rois = [None] * self.nrOfJobs
                self.Segs = [None] * self.nrOfJobs
                self.Procs = [None] * self.nrOfJobs

                # convert areas to pixel units
                areas = [area / pixelSize / pixelSize for area in [jobl['area_min'], jobl['area_max'], jobl['area_ws']]]

                # convert checkbox to boolean
                jobl['exclude_bnd'] = (jobl['exclude_bnd'] == "Yes")
                jobl['fcs_z'] = (jobl['fcs_z'] == "Yes")

                # load and show original image
                if show:
                    Zp = ZProjector()
                    Zp.setMethod(ZProjector.MAX_METHOD)
                    imgP = []
                    for img in imageC:
                        imgP.append(img.duplicate())
                    if imgP[0].getNSlices > 0:
                        for ind, img in enumerate(imgP):
                            Zp.setImage(img)
                            Zp.doProjection()
                            imgP[ind] = Zp.getProjection()
                    if len(imgP) > 1:
                        rgbmerge = RGBStackMerge()
                        imgO = rgbmerge.mergeChannels(imgP, True)
                        imgO.setLuts(LUTs)
                    else:
                        imgO = imgP[0]
                    imgO.setTitle('MAX_%s' % filename[0])
                    imgO.show()

                # switch color to Cyan for better visibility
                IJ.run(imageC[jobl['channel_seg'] - 1], "Cyan", "")

                # segment channel channel_seg and create binary
                try:
                    self.rois[ijob], self.Segs[ijob], self.Procs[ijob] = segmentChannel(imageC, areas, **jobl)
                except Exception as err:
                    self.exitWithError(str(err))
                    return
                self.Segs[ijob].setTitle(filename[0] + "_Binary")
                if self.rois[ijob] is None:
                    IJ.run("Read Write Windows Registry",
                           "action=write location=[HKCU\\%s] key=%s value=[%s] windows=REG_SZ" % (
                               zk.regkey, zk.subkey_codemic, msg.CODE_NOTHING))
                    continue

                # pick particle of interest and write commands for scope to registry
                particle, ptsFcs, self.rois[ijob] = executeTask(self.rois[ijob], self.Segs[ijob], imageC, pixelSize,
                                                                **jobl)

                if particle is None or len(particle) == 0:
                    IJ.run("Collect Garbage", "")
                    return
                # report results
                impOut = autTool.createOutputImg(self.Procs[ijob], self.rois[ijob], particle, ptsFcs)
                impOut.setTitle("Picked_particle")
                if show:
                    impOut.show()
                self.saveOutputImg(impOut, filepath, ijob + 1)
                IJ.run("Collect Garbage", "")
        except BaseException, err:
            self.exitWithError('%s at line %d' % (str(err), sys.exc_traceback.tb_lineno))


def executeTask(rois, Seg, imageC, pixelSize, **job):
    """
    Select particles in rois according intensity and position.
    Pass commands to the microscope (i.e. write entries in the windows registry)

    :param rois: are rois detected from segmentation of channel_seg
    :param Seg: binary mask obtained from segmentChannel
    :param imageC: list of ImagePlus channels (base 0!)
    :param pixelSize: pixelSize in XY
    :param job: dictionary that contains the parameters.
    :return particle:
    :return ptsFcs:
    :return rois:
    """
    try:
        zk = ZeissKeys()
        msg = Message()
        if not rois:
            IJ.run("Read Write Windows Registry", "action=write location=[HKCU\\%s] key=%s value=[%s] windows=REG_SZ" %
                   (zk.regkey, zk.subkey_codemic, msg.CODE_NOTHING))
            return None, None, None
        for i, roi in enumerate(rois):
            Seg.setRoi(roi)
            stats = Seg.getStatistics(Measurements.AREA)
            IJ.log("Found object %d with area  %.2f (um2)" % (i, stats.area * pixelSize * pixelSize))

        Zp = ZProjector()
        # Apply thresholds to image roi
        for ithr in [1, 2]:
            if job['channel_thr%d' % ithr] != "None":
                iCh = int(job['channel_thr%d' % ithr])
                if iCh > len(imageC):
                    raise Exception(
                        'Image has only %d channels. Channel for intensity threshold is %d' % (len(imageC), iCh))
                idx = list()
                imageI = imageC[iCh - 1].duplicate()
                # use central slice
                # imageI.setSlice(int(round((imageI.getNSlices() + 1) / 2)))
                # do projection
                Zp.setImage(imageI)
                Zp.setMethod(ZProjector.MAX_METHOD)
                Zp.doProjection()
                imageI = Zp.getProjection()
                for i, roi in enumerate(rois):
                    imageI.setRoi(roi)
                    stats = imageI.getStatistics(Measurements.MEAN)
                    if stats.mean >= job['intensity_min%d' % ithr] and stats.mean <= job['intensity_max%d' % ithr]:
                        idx.append(i)
                    else:
                        continue
                if len(idx) > 0:
                    rois = [rois[i] for i in idx]
                else:
                    IJ.run("Read Write Windows Registry",
                           "action=write location=[HKCU\\%s] key=%s value=[%s] windows=REG_SZ"
                           % (zk.regkey, zk.subkey_codemic, msg.CODE_NOTHING))
                    return None, None, None

        # convert some string to integers
        nrParticles = int(job['nrParticles'])
        fcs_inside = int(job['fcs_inside'])
        fcs_outside = int(job['fcs_outside'])

        particle, msg.position, areaNuc, ellipse = autTool.pick_particles(image=Seg, roiarray=rois, nrpart=nrParticles,
                                                                          mode=job['pick_method'])
        # add Z position for the different particles
        [p.append(autTool.focusParticle(imageC=imageC, channel=job['channel_seg'], roi=rois[i], method='mean')) for i, p
         in
         enumerate(msg.position)]
        ptsFcs = None

        # currently fcspts can only be specified for a single objects
        if sum([fcs_inside, fcs_outside]) > 0 and nrParticles == 1 and job['command'] in [msg.CODE_SETFCSPOS, ';'.join(
                [msg.CODE_SETFCSPOS, msg.CODE_FOCUS])]:
            Seg.deleteRoi()
            # ptsFcs = autTool.pointsOnBorder(Seg,  rois[particle], [int(job['fcs_inside']), int(job['fcs_outside'])], [15, 5], 0.5, 5)
            ptsFcs = autTool.pointsOnBorder2(Seg, rois[particle[0]], [fcs_inside, fcs_outside],
                                             map(int, [job['pixel_erode'], job['pixel_dilate']]), 0.5, 5)
            # append Z position
            [pt.append(msg.position[0][2]) for pt in ptsFcs]

        # write XYZ position to registry
        autTool.writePositionToRegistry(msg.position)

        # write coordinates of fcs points to registry
        if ptsFcs is not None:
            if len(ptsFcs) > 0:
                autTool.writeFcsPositionToRegistry(ptsFcs, job['fcs_z'])
                prefix = ';' + ';'.join(sum([['nuc'] * int(job['fcs_inside']), ['cyt'] * int(job['fcs_inside'])], []))
                # this enforces the name in the xml file
                IJ.run("Read Write Windows Registry",
                       "action=write location=[HKCU\\%s] key=prefix value=[%s] windows=REG_SZ" % (zk.regkey, prefix))

        # write command to execute to registry
        IJ.run("Read Write Windows Registry",
               "action=write location=[HKCU\\%s] key=%s value=[%s] windows=REG_SZ" % (
                   zk.regkey, zk.subkey_codemic, job['command']))
        return particle, ptsFcs, rois
    except:
        print('Error in executetask line %d' % sys.exc_traceback.tb_lineno)
        raise


def segmentChannel(imageC, areas, **job):
    '''
        Segment one channel and returns segmentation results. Use the segment_Particles() plugin, A. Politi
        Image maximally projected filtered with a median and gaussian filter of same size and thresholded.
        A watershed is performed on particles above a certain size.
        Area size selection is applied to remove very large and very small perticles
        input:
            imageC - contains all channels of the image
            areas  - a list [area_min, area_max, area_ws] in pixel units
            job    - contains parameter for segmentation. Used are channel_se, filter_rad, seg_method, and exclude_bnd (exclude_boundaries).
        output:
            rois      - rois of particles that fulfill all criteria
            imageSeg  - binary image of particles
            imageProc - image that has been processed with the filters and maximally projectd
    '''
    iCh = job['channel_seg']
    if iCh > len(imageC):
        raise IOError('Image has only %d channels. Channel for segmentation is %d' % (len(imageC), iCh))
    try:
        imageProc = imageC[iCh - 1].duplicate()
        sp = segment_Particles()
        imageProc = sp.preprocessImage(imageProc, "MAX", "C1", job['filter_rad'], job['filter_rad'])
        imageSeg = imageProc.duplicate()
        imageProc.setTitle("ProcessedCh %d" % iCh)
        imageSeg = sp.segmentParticles(imageSeg, job['seg_method'], areas[0], areas[1], areas[2], 2, job['exclude_bnd'])
        imageSeg.setTitle("SegmentedCh%d" % iCh)
        roim = RoiManager.getInstance()
        if roim == None:
            raise Exception('Fiji error automatedfcs.py: no RoiManager!')
        if roim.getCount() > 0:
            rois = roim.getRoisAsArray()
            return rois, imageSeg, imageProc
        else:
            return None, imageSeg, imageProc
    except BaseException, err:
        raise Exception('segmentChannel error' + str(err))

# somehow the name is __builtin__ instead of main when executed from the editor. If the name is changed to name_ then
# it runs
if __name__ == "__main__":
    pI = LocDialogParameters('Automated_FCS')

